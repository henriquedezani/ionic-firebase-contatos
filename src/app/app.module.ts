import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListaPage } from './../pages/lista/lista';
import { CadastroPage } from './../pages/cadastro/cadastro';
import { EditaPage } from './../pages/edita/edita';
import { LoginPage } from '../pages/login/login';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { FormsModule } from '@angular/forms';

import { Camera } from '@ionic-native/camera';

const config = {
  apiKey: "AIzaSyASsDNv9Ts-UF9N9KwHkH0GrRb5soYr9Hk",
  authDomain: "contatosfirebase-25908.firebaseapp.com",
  databaseURL: "https://contatosfirebase-25908.firebaseio.com",
  projectId: "contatosfirebase-25908",
  storageBucket: "contatosfirebase-25908.appspot.com",
  messagingSenderId: "641337937135"
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListaPage,
    CadastroPage,
    EditaPage,
    LoginPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    FormsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListaPage,
    CadastroPage,
    EditaPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Camera
  ]
})
export class AppModule {}

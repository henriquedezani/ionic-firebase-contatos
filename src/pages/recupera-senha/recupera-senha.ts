import { AngularFireAuth } from '@angular/fire/auth';
import { NgForm } from '@angular/forms';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recupera-senha',
  templateUrl: 'recupera-senha.html',
})
export class RecuperaSenhaPage {

  constructor(public navCtrl: NavController,
     public navParams: NavParams,
     public afAuth: AngularFireAuth) {
  }

  public recuperar(form: NgForm) {
    let email = form.value.email;

    this.afAuth.auth.sendPasswordResetEmail(email)
      .then(() => {
        this.navCtrl.pop();
      })
      .catch((error) => {
        alert(error);
      })

  }
}

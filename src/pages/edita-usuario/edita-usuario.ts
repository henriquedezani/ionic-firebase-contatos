import { NgForm } from '@angular/forms';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-edita-usuario',
  templateUrl: 'edita-usuario.html',
})
export class EditaUsuarioPage {

  public nome: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public afAuth: AngularFireAuth) {
  }

  ionViewDidEnter(){
   this.nome = this.afAuth.auth.currentUser.displayName;
  }

  public salvar(form: NgForm) {

    this.afAuth.auth.currentUser.updateProfile({
      displayName:  form.value.nome,
      photoURL: ''
    })
    .then(() => {
      this.navCtrl.pop();
    })
    .catch((error) => {
      alert(error);
    })

  }

}
